# nginx server security group (AKA firewall) definition
resource "aws_security_group" "nginx-security-group" {
  name        = "nginx-security-group"

  # inbound traffic
  ingress {
    from_port     = 80
    to_port       = 80
    protocol      = "tcp"
    cidr_blocks   = ["0.0.0.0/0"]
  }
  ingress {
    from_port     = 22
    to_port       = 22
    protocol      = "tcp"
    cidr_blocks   = ["0.0.0.0/0"]
  }

  # outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# nginx server definition
resource "aws_instance" "nginx-server" {
  ami               = "ami-01e24be29428c15b2"
  instance_type     = "t3.micro"
  availability_zone = "us-west-2a"
  key_name          = "tranque-dev-us-west-2"
  security_groups   = ["${aws_security_group.nginx-security-group.name}", "default"]

  tags {
    Name = "nginx-server"
    Role = "web-servers"
  }
}
