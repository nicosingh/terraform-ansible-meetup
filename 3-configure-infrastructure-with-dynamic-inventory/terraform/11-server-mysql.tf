# mysql server security group (AKA firewall) definition
resource "aws_security_group" "mysql-security-group" {
  name        = "mysql-security-group"

  # inbound traffic
  ingress {
    from_port         = 3306
    to_port           = 3306
    protocol          = "tcp"
    security_groups   = ["${aws_security_group.nginx-security-group.id}"]
  }
  ingress {
    from_port     = 22
    to_port       = 22
    protocol      = "tcp"
    cidr_blocks   = ["0.0.0.0/0"]
  }

  # outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# mysql server definition
resource "aws_instance" "mysql-server" {
  ami               = "ami-01e24be29428c15b2"
  instance_type     = "t3.micro"
  availability_zone = "us-west-2a"
  key_name          = "tranque-dev-us-west-2"
  security_groups   = ["${aws_security_group.mysql-security-group.name}", "default"]

  tags {
    Name = "mysql-server"
    Role = "db-servers"
  }
}
