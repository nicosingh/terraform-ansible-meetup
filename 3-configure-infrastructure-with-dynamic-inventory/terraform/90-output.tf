# nginx server public IP address
output "nginx-public-ip" {
  value = "${aws_instance.nginx-server.public_ip}"
}

# mysql server public IP address
output "mysql-public-ip" {
  value = "${aws_instance.mysql-server.public_ip}"
}
