#!/bin/bash

cd terraform
terraform init
terraform apply -auto-approve

cd ../ansible
aws s3 cp s3://terraform-ansible-meetup-tfstate/terraform.tfstate .
TF_STATE=./terraform.tfstate ansible-playbook --ssh-common-args="-o StrictHostKeyChecking=no" --private-key=~/.ssh/tranque-dev-us-west-2.pem --inventory-file=/usr/local/bin/terraform-inventory *-*.yml
rm -f terraform.tfstate
