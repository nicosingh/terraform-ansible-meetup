provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "terraform-ansible-meetup-tfstate"
    key    = "terraform.tfstate"
    region = "us-west-2"
  }
}
