#!/bin/bash
cd terraform
terraform init
terraform apply -auto-approve

cd ../ansible
ansible-playbook --ssh-common-args="-o StrictHostKeyChecking=no" --private-key=~/.ssh/tranque-dev-us-west-2.pem --inventory-file=inventory.yml *-*.yml
